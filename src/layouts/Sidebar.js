import React from "react";
import fotodiri from "../img/fifi.jpg";
import { Link } from "react-router-dom";
const Sidebar = () => {
  return (
    <>
      <div className="Sidebar">
        <h1>About Me</h1>
        <img src={fotodiri} />
        <p>
          <strong>Rizka Fitriani</strong>
        </p>
        <p>
          A proffesional movie and game reviewer.
        </p>
      </div>
    </>
  );
};

export default Sidebar;
