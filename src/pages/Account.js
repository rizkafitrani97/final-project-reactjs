import React, { useState, useContext } from "react";
import axios from "axios";
import UserContext from "../context/UserContext";
import { useHistory } from "react-router-dom";
import { Button, TextField, makeStyles } from "@material-ui/core";
const Account = () => {
  const [account, setAccount] = useContext(UserContext);

  const [input, setInput] = useState({
    oldPassword: "",
    newPassword: "",
  });

  const handleChange = (event) => {
    let value = event.target.value;
    let name = event.target.name;
    switch (name) {
      case "oldPassword":
        setInput({ ...input, oldPassword: value });
        break;
      case "newPassword":
        setInput({ ...input, newPassword: value });
        break;
      default: {
        break;
      }
    }
  };
  const history = useHistory();
  const handleLogout = () => {
    setAccount(null);
    localStorage.removeItem("user");
    history.push("/login");
  };

  const handleChangePassword = (event) => {
    event.preventDefault();

    if (input.oldPassword === account.password) {
      axios
        .put(`https://backendexample.sanbersy.com/api/users/${account.id}`, {
          ...account,
          password: input.newPassword,
        })
        .then((res) => {
          setAccount(res.data);
        });

      alert("Your password is successfully changed!");
    } else {
      alert("Your old password is wrong!");
    }
    setInput({ ...input, oldPassword: "", newPassword: "" });
  };
  const useStyles = makeStyles((theme) => ({
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(1),
        width: "25ch",
      },
      "& label.Mui-focused": {
        color: "black",
      },
      "& .MuiInput-underline:after": {
        borderBottomColor: "black",
      },
      "& .MuiOutlinedInput-root": {
        "& fieldset": {
          borderColor: "black",
        },
        "&:hover fieldset": {
          borderColor: "black",
        },
        "&.Mui-focused fieldset": {
          borderColor: "black",
        },
      },
    },
    input: {
      color: "black",
    },
    floatingLabelFocusStyle: {
      color: "gray",
    },
  }));
  const classes = useStyles();
  return (
    <div className="content">
      <h1>Account Page</h1>
      <div className="changePassword">
        <h2>Change Password:</h2>
        <form
          className="loginForm"
          className={classes.root}
          onSubmit={handleChangePassword}
        >
          <label>Old Password: </label>
          <TextField
            InputLabelProps={{
              className: classes.floatingLabelFocusStyle,
            }}
            InputProps={{
              className: classes.input,
            }}
            variant="outlined"
            label="Old Password"
            type="text"
            name="oldPassword"
            onChange={handleChange}
            value={input.oldPassword}
          />
          <br />
          <label>New Password: </label>
          <TextField
            InputLabelProps={{
              className: classes.floatingLabelFocusStyle,
            }}
            InputProps={{
              className: classes.input,
            }}
            variant="outlined"
            label="New Password"
            type="password"
            name="newPassword"
            onChange={handleChange}
            value={input.newPassword}
          />
          <br />
          <Button
            variant="contained"
            color="primary"
            style={{ marginTop: "20px" }}
            type="Submit"
          >
            Change Password
          </Button>
        </form>
        <Button
          onClick={handleLogout}
          variant="contained"
          color="secondary"
          style={{ marginTop: "20px" }}
          type="Submit"
        >
          Logout
        </Button>
      </div>
    </div>
  );
};

export default Account;
